package com.example.zenhwork.service;

import com.example.zenhwork.model.Central;
import com.example.zenhwork.repository.CentralRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CentralService extends ResourceSupport{

  private final CentralRepository centralRepository;

  @Autowired
  CentralService(CentralRepository centralRepository){
    this.centralRepository = centralRepository;
  }

  // add a central
  public Central add(Central central){
    return centralRepository.save(central);
  }

  // retrieve one central by id
  public Central findByid(long id) {
    return centralRepository.findOne(id);
  }

  // retrieve all centrals
  public List<Central> findAll() {
    return centralRepository.findAll();
  }


}
