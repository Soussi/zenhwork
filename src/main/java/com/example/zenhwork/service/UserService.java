package com.example.zenhwork.service;

import com.example.zenhwork.model.User;
import com.example.zenhwork.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Service
public class UserService extends ResourceSupport{

  private final UserRepository userRepository;

  @Autowired
  UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }


  public User addOrUpdate(User user) {

    return userRepository.save(user);
  }

  public void modifyUser(@RequestParam String firstName, @RequestParam String lastName, @RequestParam String password, @RequestParam String email, User userToUpdate) {
    userToUpdate.setFirstName(firstName);
    userToUpdate.setLastName(lastName);
    userToUpdate.setPassword(password);
    userToUpdate.setEmail(email);
    addOrUpdate(userToUpdate);
  }

  public User findById(Integer id) {
    return userRepository.findOne(id);
  }

  public Optional<User> findOneByEmail(String email) {
    return userRepository.findOne(email);
  }

  public List<User> getAllUsers() {
    return userRepository.findAll();
  }

  public  boolean isUserExists(User user) {
    return findOneByEmail(user.getEmail()) != null;
  }
}
