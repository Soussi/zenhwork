package com.example.zenhwork.controller;

import com.example.zenhwork.model.Central;
import com.example.zenhwork.model.User;
import com.example.zenhwork.service.CentralService;
import com.example.zenhwork.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api")
public class ApiController {

  private User user4 = new User("Salah", "Outemsaa", "sou1@gmail.com", "1234", true, "ADMIN");
  private final Central centralDemo = new Central("Solary", 10000, 2000, user4);
  private  final Central centralDemoOther  = new Central("Thermo", 20000, 5000, user4);

  private static final Logger logger = LoggerFactory.getLogger(ApiController.class);
  private final UserService userService;
  private final CentralService centralService;

  @Autowired
  ApiController(UserService userService, CentralService centralService) {
    this.userService = userService;
    this.centralService = centralService;
  }

  @GetMapping("/test")
  public String hello(){
    return "It's working :)";
  }

  @RequestMapping(value ="/user/current", method = RequestMethod.GET)
  public String getCurrentuser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String currentPrincipal =  authentication.getPrincipal().toString();

    Link selfLink = linkTo(ApiController.class).slash("user").slash(currentPrincipal).withSelfRel();
    return currentPrincipal;
  }


  @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
  public ResponseEntity<?> getUser(@PathVariable("id") int id) {
    logger.info("Fetching User with id {}", id);
    User user = userService.findById(id);
    if (user == null) {
      logger.error("User with id {} not found.", id);
      return new ResponseEntity(HttpStatus.NOT_FOUND);

    }
    Link selfLink = linkTo(ApiController.class).slash("user").slash(user.getUserId()).withSelfRel();
    user.add(selfLink);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @RequestMapping(path = "/users", method = RequestMethod.GET)
  public List<User> getUsers() {
    List<User> users = userService.getAllUsers();

    for(User user: users) {
      Link selfLink = linkTo(ApiController.class).slash("user").slash(user.getUserId()).withSelfRel();
      user.add(selfLink);
    }
    return users;
  }

  @RequestMapping(path = "/user/add", method = RequestMethod.GET)
  public ModelAndView addUser(Model model) {

    User user4 = new User("Salah", "Outemsaa", "sou1@gmail.com", "1234", true, "ADMIN");
    User user5 = new User("Sa", "OU", "sou2@gmail.com", "1234", true, "USER");
    User user6 = new User("Sah", "Outa", "sou3@gmail.com", "1234", true, "USER");

    List<User> userList = Arrays.asList(user4, user5, user6);
    Map<String, User> usersForDemo =  userList.stream().map(user -> userService.addOrUpdate(user)).collect(Collectors.toMap(
      User::getEmail,
      Function.identity()
    ));

    model.addAllAttributes(usersForDemo);

    return new ModelAndView("redirect:/api/users");
  }

  @RequestMapping(path = "/user/new", method = RequestMethod.POST)
  public ResponseEntity createAccount(@RequestBody User user, UriComponentsBuilder uriComponentsBuilder) {

    logger.info("Création d'un nouveau compte", user);

    if(userService.isUserExists(user)) {
      logger.error("Impossible de s'enregistrer. Cet email {} existe déjà: " , user.getEmail());
    }
    userService.addOrUpdate(user);

    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(uriComponentsBuilder.path("/api/user/{id}").buildAndExpand(user.getId()).toUri());
    return new ResponseEntity<String> (headers, HttpStatus.CREATED);
  }

  // Add some central for demo
  @RequestMapping(path = "/central/add", method = RequestMethod.GET)
  public ModelAndView addSomeCentralsForDemo(Model model) {


    User user4 = new User("Salah", "Outemsaa", "sou1@gmail.com", "1234", true, "ADMIN");
    Central centralDemo = new Central("Solar", 10000, 2000, user4);
    Central centralDemoOther  = new Central("Thermo", 20000, 5000, user4);

    List<Central> centralsList = Arrays.asList(centralDemo, centralDemoOther);
    List<Central> centralsForDemo =  centralsList.stream().map(central -> centralService.add(central)).collect(Collectors.toList());

    model.addAllAttributes(centralsForDemo);

    return new ModelAndView("redirect:/api/centrals");
  }

  @RequestMapping(path = "/centrals", method = RequestMethod.GET)
  public List<Central> getCentrals() {

    List<Central> centrals = centralService.findAll();

    return centrals.stream().peek(central -> {
      Link selfLink = linkTo(ApiController.class).slash("/centrals").slash(central.getCentralId()).withSelfRel();
      central.add(selfLink);
    }).collect(Collectors.toList());

  }

  @RequestMapping(path = "/centrals/{id}")
  public Central showCentral(@PathVariable int id) {
    Link selfLink = linkTo(ApiController.class).slash("/centrals").slash(centralService.findByid(id).getCentralId()).withSelfRel();

    return centralService.findByid(id);
  }

  // Add a new power plant
  @RequestMapping(path = "/central/new", method = RequestMethod.POST)
  public ResponseEntity createNewPowerPlant(@RequestBody Central central) {

    centralService.add(central);

    return new ResponseEntity(HttpStatus.CREATED);
  }

}
