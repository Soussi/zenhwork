package com.example.zenhwork.controller;

import com.example.zenhwork.model.User;
import com.example.zenhwork.service.UserService;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

  @Autowired
  UserService userService;

  @RequestMapping(value = "/registration")
  public String showRegistrationForm() {
    return "registration";
  }

  @RequestMapping(path = "/account/create")
  public ModelAndView addAccount(@RequestParam String firstName,
                              @RequestParam String lastName,
                              @RequestParam String email,
                              @RequestParam String password,
                              ModelMap model) {

    String role = "USER";
    User user = new User(firstName, lastName, email, password, true, role);
    userService.addOrUpdate(user);
    return new ModelAndView("redirect:/");
  }



}
