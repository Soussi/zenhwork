package com.example.zenhwork.repository;

import com.example.zenhwork.model.Central;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CentralRepository extends JpaRepository<Central, Long> {

}
