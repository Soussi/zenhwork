package com.example.zenhwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenhworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenhworkApplication.class, args);

	}
}
