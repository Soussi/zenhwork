package com.example.zenhwork.model;

import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;

@Entity
public class Central extends ResourceSupport{

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int centralId;

  private  String nature;
  private int  capacity;
  private int energy;

  @ManyToOne(cascade = CascadeType.ALL)
  private User user;

  Central() {

  }

  public Central(String nature, int capacity, int energy, User user) {
    this.nature = nature;
    this.capacity = capacity;
    this.energy = energy;
    this.user = user;
  }

  public int getCentralId() {
    return this.centralId;
  }

  public String getNature() {
    return this.nature;
  }

  public int getCapacity() {
    return this.capacity;
  }

  public int getEnergy() {
    return this.energy;
  }

  public User getUser() {
    return this.user;
  }
}
