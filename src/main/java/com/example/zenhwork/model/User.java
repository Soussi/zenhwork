package com.example.zenhwork.model;

import org.hibernate.validator.constraints.Email;
import org.springframework.hateoas.ResourceSupport;

import javax.management.relation.Role;
import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "user")
public class User  extends ResourceSupport implements Serializable{


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int userId;

  private String firstName;

  private String lastName;

  @Column(name = "email", unique = true, nullable = false)
  @Email(message = "Format message n'est pas valide")
  private String email;

  @Column(nullable = false)
  private String password;

  private boolean enabled;

  private String role;

  @OneToMany
  @JoinColumn(name = "user_id")
  private Set<Central> centrals = new HashSet<>();

  public User() {
    // For Hibernate
  }

  public User(String firstName, String lastName, String email, String password, boolean enabled, String role) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.enabled = enabled;
    this.role = role;
  }

  public int getUserId() {
    return userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public String getRole() {
    return role;
 }

  public Set<Central> getCentrals() {
    return centrals;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public void setRole(String role) {
    this.role = role;
  }
}
